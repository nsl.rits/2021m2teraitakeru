/*
Copyright (c) 1997, 1998 Carnegie Mellon University.  All Rights
Reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The AODV code developed by the CMU/MONARCH group was optimized and tuned by Samir Das and Mahesh Marina, University of Cincinnati. The work was partially done in Sun Microsystems. Modified for gratuitous replies by Anant Utgikar, 09/16/02.

*/

//#include <ip.h>

#include <aodv/aodv.h>
#include <aodv/aodv_packet.h>
#include <random.h>
#include <cmu-trace.h>
//#include <energy-model.h>

#include <iostream>
#include <fstream>

#define max(a,b)        ( (a) > (b) ? (a) : (b) )
#define CURRENT_TIME    Scheduler::instance().clock()

//攻撃ノード設定
#define A_1 48
#define A_2 49

//#define DEBUG
//#define ERROR

//ノード数
#define node_num 50
//共有すう
#define share_num 5

int malicious;

double seq[node_num][node_num][200]={0}; //近似用シーケンス番号の格納
double timer[node_num][node_num][200]={0}; //シーケンス受け取ってからの経過時間
double timer_first[node_num][node_num]={0}; //シーケンス番号を初めて受け取った時間
double timer_first_a[node_num][node_num]={0}; //攻撃用
int count[node_num][200]={0}; //各ノードが持っているシーケンス番号の個数
int count_s[node_num][node_num]={0}; //何回送信元にシーケンス送信したか

double a_t_success[2]={0,0}; //攻撃成功率判定用
double n_t_success[2]={0,0}; //ご判定率判定用
int flag=1; //攻撃成功率判定に使用、同じ宛先に重複して送るパケットを数えないために
int rrep_recv_flag[node_num][node_num]={0};//RREP受信の有無格納フラグ
int rrep_recv_or_not=0; //送信元が判定する、RREPを受け取ったかどうかのフラグ。これが0の時、受け取っておらず、閾値防御法の処理に入る

double recv_replist[node_num][node_num][200]={0}; //RREPの送信とタイムスタンプのリスト 第一要素:所持するノードアドレス 第2要素:判定先ノードアドレス 第3要素:何個目のデータか 中身:RREPを送信したタイムスタンプ 基本的にこのシナリオでは全ノードが使用する
int recv_rep_numlist[node_num][node_num]={0}; //RREP送信数リスト 第一要素:所持するノードアドレス 第2要素:判定先ノードアドレス 中身:RREP受信数 基本的にこのシナリオではノード0が使用する
int index_count[node_num][node_num][node_num]={0}; //情報共有の際に使用 第一引数のノードアドレスが第2引数のノードアドレスの情報をどこまで参照したか recv_replistを参照するために使用(今回がノード0で判定するためどのノードに対する情報共有かは明示していない,全ノードが判定するなら，どのノードアドレスに対して情報共有したかというのをあらわ表す配列の要素がもう1つ必要)
int recv_rep_num_sortedlist[node_num][node_num]={0};//ソート後を格納する
int ranking=0; //ソートに使う

double total_recv=0; //受け取った総RREP数
double distinc_attack=0; //そのうち、いくら攻撃ノード判別されたか

int num=4; //閾値判別に用いる余分の値

int a_share_count[node_num]={0};
double min_recv_time[10]={0};
int min_recv_time_temp=0;
int flag2=0;

#ifdef DEBUG
static int route_request = 0;
#endif

const char *fileName1 = "攻撃.csv";
const char *fileName2 = "誤判定.csv";
ofstream outputfile(fileName1,ios_base::out|ios_base::app);
ofstream outputfile2(fileName2,ios_base::out|ios_base::app);

class MyAgent : public Agent{
    public:
    MyAgent();
    protected:
    int command(int argc, const char *const *argv);
    private:
    void Myfunc();
    int malicious_temp;
};

static class MyAgentClass : public TclClass{
    public:
    MyAgentClass() : TclClass("Agent/MyAgent"){}
    TclObject* create(int, const char*const*){
        return (new MyAgent());
    }
} class_my_agent;

MyAgent::MyAgent() : Agent(PT_UDP){
    bind("malicious_1",&malicious_temp);
}

int MyAgent::command(int argc,const char*const* argv){
    if(argc==2){
        if(strcmp(argv[1],"myfunc")==0){
            Myfunc();
            return TCL_OK;
        }
        return (Agent::command(argc,argv));
    }
}

void MyAgent::Myfunc(){
    malicious=malicious_temp;
    //fprintf(stdout,"%d",malicious);
    outputfile<<"\n\n";
    
    outputfile2<<"\n\n";
}


/*
TCL Hooks
*/

class IOFile{
    public:
    IOFile(){
    }
};

IOFile iofile;



int hdr_aodv::offset_;
static class AODVHeaderClass : public PacketHeaderClass {
    public:
    AODVHeaderClass() : PacketHeaderClass("PacketHeader/AODV",
    sizeof(hdr_all_aodv)) {
        bind_offset(&hdr_aodv::offset_);
    }
} class_rtProtoAODV_hdr;

static class AODVclass : public TclClass {
    public:
    AODVclass() : TclClass("Agent/AODV") {}
    TclObject* create(int argc, const char*const* argv) {
        assert(argc == 5);
        //return (new AODV((nsaddr_t) atoi(argv[4])));
        return (new AODV((nsaddr_t) Address::instance().str2addr(argv[4])));
    }
} class_rtProtoAODV;


int
AODV::command(int argc, const char*const* argv) {
    if(argc == 2) {
        Tcl& tcl = Tcl::instance();
        
        if(strncasecmp(argv[1], "id", 2) == 0) {
            tcl.resultf("%d", index);
            return TCL_OK;
        }
        
        if(strncasecmp(argv[1], "start", 2) == 0) {
            btimer.handle((Event*) 0);
            
            #ifndef AODV_LINK_LAYER_DETECTION
            htimer.handle((Event*) 0);
            ntimer.handle((Event*) 0);
            #endif // LINK LAYER DETECTION
            
            rtimer.handle((Event*) 0);
            return TCL_OK;
        }
    }
    else if(argc == 3) {
        if(strcmp(argv[1], "index") == 0) {
            index = atoi(argv[2]);
            return TCL_OK;
        }
        
        else if(strcmp(argv[1], "log-target") == 0 || strcmp(argv[1], "tracetarget") == 0) {
            logtarget = (Trace*) TclObject::lookup(argv[2]);
            if(logtarget == 0)
            return TCL_ERROR;
            return TCL_OK;
        }
        else if(strcmp(argv[1], "drop-target") == 0) {
            int stat = rqueue.command(argc,argv);
            if (stat != TCL_OK) return stat;
            return Agent::command(argc, argv);
        }
        else if(strcmp(argv[1], "if-queue") == 0) {
            ifqueue = (PriQueue*) TclObject::lookup(argv[2]);
            
            if(ifqueue == 0)
            return TCL_ERROR;
            return TCL_OK;
        }
        else if (strcmp(argv[1], "port-dmux") == 0) {
            dmux_ = (PortClassifier *)TclObject::lookup(argv[2]);
            if (dmux_ == 0) {
                fprintf (stderr, "%s: %s lookup of %s failed\n", __FILE__,
                argv[1], argv[2]);
                return TCL_ERROR;
            }
            return TCL_OK;
        }
    }
    return Agent::command(argc, argv);
}

/*
Constructor
*/

AODV::AODV(nsaddr_t id) : Agent(PT_AODV),
btimer(this), htimer(this), ntimer(this),
rtimer(this), lrtimer(this), rqueue() {
    
    
    index = id;
    seqno = 2;
    bid = 1;
    
    LIST_INIT(&nbhead);
    LIST_INIT(&bihead);
    
    logtarget = 0;
    ifqueue = 0;
}

/*
Timers
*/

void
BroadcastTimer::handle(Event*) {
    agent->id_purge();
    Scheduler::instance().schedule(this, &intr, BCAST_ID_SAVE);
}

void
HelloTimer::handle(Event*) {
    agent->sendHello();
    double interval = MinHelloInterval +
    ((MaxHelloInterval - MinHelloInterval) * Random::uniform());
    assert(interval >= 0);
    Scheduler::instance().schedule(this, &intr, interval);
}

void
NeighborTimer::handle(Event*) {
    agent->nb_purge();
    Scheduler::instance().schedule(this, &intr, HELLO_INTERVAL);
}

void
RouteCacheTimer::handle(Event*) {
    agent->rt_purge();
    #define FREQUENCY 0.5 // sec
    Scheduler::instance().schedule(this, &intr, FREQUENCY);
}

void
LocalRepairTimer::handle(Event* p)  {  // SRD: 5/4/99
    aodv_rt_entry *rt;
    struct hdr_ip *ih = HDR_IP( (Packet *)p);
    
    /* you get here after the timeout in a local repair attempt */
    /*    fprintf(stderr, "%s\n", __FUNCTION__); */
    
    
    rt = agent->rtable.rt_lookup(ih->daddr());
    
    if (rt && rt->rt_flags != RTF_UP) {
        // route is yet to be repaired
        // I will be conservative and bring down the route
        // and send route errors upstream.
        /* The following assert fails, not sure why */
        /* assert (rt->rt_flags == RTF_IN_REPAIR); */
        
        //rt->rt_seqno++;
        agent->rt_down(rt);
        // send RERR
        #ifdef DEBUG
        fprintf(stderr,"Dst - %d, failed local repair\n", rt->rt_dst);
        #endif
    }
    Packet::free((Packet *)p);
}


/*
Broadcast ID Management  Functions
*/


void
AODV::id_insert(nsaddr_t id, u_int32_t bid) {
    BroadcastID *b = new BroadcastID(id, bid);
    
    assert(b);
    b->expire = CURRENT_TIME + BCAST_ID_SAVE;
    LIST_INSERT_HEAD(&bihead, b, link);
}

/* SRD */
bool
AODV::id_lookup(nsaddr_t id, u_int32_t bid) {
    BroadcastID *b = bihead.lh_first;
    
    // Search the list for a match of source and bid
    for( ; b; b = b->link.le_next) {
        if ((b->src == id) && (b->id == bid))
        return true;
    }
    return false;
}

void
AODV::id_purge() {
    BroadcastID *b = bihead.lh_first;
    BroadcastID *bn;
    double now = CURRENT_TIME;
    
    for(; b; b = bn) {
        bn = b->link.le_next;
        if(b->expire <= now) {
            LIST_REMOVE(b,link);
            delete b;
        }
    }
}

/*
Helper Functions
*/

double
AODV::PerHopTime(aodv_rt_entry *rt) {
    int num_non_zero = 0, i;
    double total_latency = 0.0;
    
    if (!rt)
    return ((double) NODE_TRAVERSAL_TIME );
    
    for (i=0; i < MAX_HISTORY; i++) {
        if (rt->rt_disc_latency[i] > 0.0) {
            num_non_zero++;
            total_latency += rt->rt_disc_latency[i];
        }
    }
    if (num_non_zero > 0)
    return(total_latency / (double) num_non_zero);
    else
    return((double) NODE_TRAVERSAL_TIME);
    
}

/*
Link Failure Management Functions
*/

static void
aodv_rt_failed_callback(Packet *p, void *arg) {
    ((AODV*) arg)->rt_ll_failed(p);
}

/*
* This routine is invoked when the link-layer reports a route failed.
*/
void
AODV::rt_ll_failed(Packet *p) {
    struct hdr_cmn *ch = HDR_CMN(p);
    struct hdr_ip *ih = HDR_IP(p);
    aodv_rt_entry *rt;
    nsaddr_t broken_nbr = ch->next_hop_;
    
    #ifndef AODV_LINK_LAYER_DETECTION
    drop(p, DROP_RTR_MAC_CALLBACK);
    #else
    
    /*
    * Non-data packets and Broadcast Packets can be dropped.
    */
    if(! DATA_PACKET(ch->ptype()) ||
    (u_int32_t) ih->daddr() == IP_BROADCAST) {
        drop(p, DROP_RTR_MAC_CALLBACK);
        return;
    }
    log_link_broke(p);
    if((rt = rtable.rt_lookup(ih->daddr())) == 0) {
        drop(p, DROP_RTR_MAC_CALLBACK);
        return;
    }
    log_link_del(ch->next_hop_);
    
    #ifdef AODV_LOCAL_REPAIR
    /* if the broken link is closer to the dest than source,
    attempt a local repair. Otherwise, bring down the route. */
    
    
    if (ch->num_forwards() > rt->rt_hops) {
        local_rt_repair(rt, p); // local repair
        // retrieve all the packets in the ifq using this link,
        // queue the packets for which local repair is done,
        return;
    }
    else
    #endif // LOCAL REPAIR
    
    {
        drop(p, DROP_RTR_MAC_CALLBACK);
        // Do the same thing for other packets in the interface queue using the
        // broken link -Mahesh
        while((p = ifqueue->filter(broken_nbr))) {
            drop(p, DROP_RTR_MAC_CALLBACK);
        }
        nb_delete(broken_nbr);
    }
    
    #endif // LINK LAYER DETECTION
}

void
AODV::handle_link_failure(nsaddr_t id) {
    aodv_rt_entry *rt, *rtn;
    Packet *rerr = Packet::alloc();
    struct hdr_aodv_error *re = HDR_AODV_ERROR(rerr);
    
    re->DestCount = 0;
    for(rt = rtable.head(); rt; rt = rtn) {  // for each rt entry
        rtn = rt->rt_link.le_next;
        if ((rt->rt_hops != INFINITY2) && (rt->rt_nexthop == id) ) {
            assert (rt->rt_flags == RTF_UP);
            assert((rt->rt_seqno%2) == 0);
            rt->rt_seqno++;
            re->unreachable_dst[re->DestCount] = rt->rt_dst;
            re->unreachable_dst_seqno[re->DestCount] = rt->rt_seqno;
            #ifdef DEBUG
            fprintf(stderr, "%s(%f): %d\t(%d\t%u\t%d)\n", __FUNCTION__, CURRENT_TIME,
            index, re->unreachable_dst[re->DestCount],
            re->unreachable_dst_seqno[re->DestCount], rt->rt_nexthop);
            #endif // DEBUG
            re->DestCount += 1;
            rt_down(rt);
        }
        // remove the lost neighbor from all the precursor lists
        rt->pc_delete(id);
    }
    
    if (re->DestCount > 0) {
        #ifdef DEBUG
        fprintf(stderr, "%s(%f): %d\tsending RERR...\n", __FUNCTION__, CURRENT_TIME, index);
        #endif // DEBUG
        sendError(rerr, false);
    }
    else {
        Packet::free(rerr);
    }
}

void
AODV::local_rt_repair(aodv_rt_entry *rt, Packet *p) {
    #ifdef DEBUG
    fprintf(stderr,"%s: Dst - %d\n", __FUNCTION__, rt->rt_dst);
    #endif
    // Buffer the packet
    rqueue.enque(p);
    
    // mark the route as under repair
    rt->rt_flags = RTF_IN_REPAIR;
    
    sendRequest(rt->rt_dst);
    
    // set up a timer interrupt
    Scheduler::instance().schedule(&lrtimer, p->copy(), rt->rt_req_timeout);
}

void
AODV::rt_update(aodv_rt_entry *rt, u_int32_t seqnum, u_int16_t metric,
nsaddr_t nexthop, double expire_time) {
    
    rt->rt_seqno = seqnum;
    rt->rt_hops = metric;
    rt->rt_flags = RTF_UP;
    rt->rt_nexthop = nexthop;
    rt->rt_expire = expire_time;
}

void
AODV::rt_down(aodv_rt_entry *rt) {
    /*
    *  Make sure that you don't "down" a route more than once.
    */
    
    if(rt->rt_flags == RTF_DOWN) {
        return;
    }
    
    // assert (rt->rt_seqno%2); // is the seqno odd?
    rt->rt_last_hop_count = rt->rt_hops;
    rt->rt_hops = INFINITY2;
    rt->rt_flags = RTF_DOWN;
    rt->rt_nexthop = 0;
    rt->rt_expire = 0;
    
} /* rt_down function */

/*
Route Handling Functions
*/

void
AODV::rt_resolve(Packet *p) {
    struct hdr_cmn *ch = HDR_CMN(p);
    struct hdr_ip *ih = HDR_IP(p);
    aodv_rt_entry *rt;
    
    /*
    *  Set the transmit failure callback.  That
    *  won't change.
    */
    ch->xmit_failure_ = aodv_rt_failed_callback;
    ch->xmit_failure_data_ = (void*) this;
    rt = rtable.rt_lookup(ih->daddr());
    if(rt == 0) {
        rt = rtable.rt_add(ih->daddr());
    }
    
    /*
    * If the route is up, forward the packet
    */
    
    if(rt->rt_flags == RTF_UP) {
        assert(rt->rt_hops != INFINITY2);
        
        /*fprintf(stdout,"123\n");
        if(ih->saddr()==0&&rt->rt_nexthop==3){
            a_t_success[0]=a_t_success[0]-1;
        }*/
        forward(rt, p, NO_DELAY);
    }
    /*
    *  if I am the source of the packet, then do a Route Request.
    */
    else if(ih->saddr() == index) {
        rqueue.enque(p);
        sendRequest(rt->rt_dst);
    }
    /*
    *    A local repair is in progress. Buffer the packet.
    */
    else if (rt->rt_flags == RTF_IN_REPAIR) {
        rqueue.enque(p);
    }
    
    /*
    * I am trying to forward a packet for someone else to which
    * I don't have a route.
    */
    else {
        Packet *rerr = Packet::alloc();
        struct hdr_aodv_error *re = HDR_AODV_ERROR(rerr);
        /*
        * For now, drop the packet and send error upstream.
        * Now the route errors are broadcast to upstream
        * neighbors - Mahesh 09/11/99
        */
        
        assert (rt->rt_flags == RTF_DOWN);
        re->DestCount = 0;
        re->unreachable_dst[re->DestCount] = rt->rt_dst;
        re->unreachable_dst_seqno[re->DestCount] = rt->rt_seqno;
        re->DestCount += 1;
        #ifdef DEBUG
        fprintf(stderr, "%s: sending RERR...\n", __FUNCTION__);
        #endif
        sendError(rerr, false);
        
        drop(p, DROP_RTR_NO_ROUTE);
    }
    
}

void
AODV::rt_purge() {
    aodv_rt_entry *rt, *rtn;
    double now = CURRENT_TIME;
    double delay = 0.0;
    Packet *p;
    
    for(rt = rtable.head(); rt; rt = rtn) {  // for each rt entry
        rtn = rt->rt_link.le_next;
        if ((rt->rt_flags == RTF_UP) && (rt->rt_expire < now)) {
            // if a valid route has expired, purge all packets from
            // send buffer and invalidate the route.
            assert(rt->rt_hops != INFINITY2);
            while((p = rqueue.deque(rt->rt_dst))) {
                #ifdef DEBUG
                fprintf(stderr, "%s: calling drop()\n",
                __FUNCTION__);
                #endif // DEBUG
                drop(p, DROP_RTR_NO_ROUTE);
            }
            rt->rt_seqno++;
            assert (rt->rt_seqno%2);
            rt_down(rt);
        }
        else if (rt->rt_flags == RTF_UP) {
            // If the route is not expired,
            // and there are packets in the sendbuffer waiting,
            // forward them. This should not be needed, but this extra
            // check does no harm.
            assert(rt->rt_hops != INFINITY2);
            while((p = rqueue.deque(rt->rt_dst))) {
                forward (rt, p, delay);
                delay += ARP_DELAY;
            }
        }
        else if (rqueue.find(rt->rt_dst))
        // If the route is down and
        // if there is a packet for this destination waiting in
        // the sendbuffer, then send out route request. sendRequest
        // will check whether it is time to really send out request
        // or not.
        // This may not be crucial to do it here, as each generated
        // packet will do a sendRequest anyway.
        
        sendRequest(rt->rt_dst);
    }
    
}

/*
Packet Reception Routines
*/

void
AODV::recv(Packet *p, Handler*) {
    struct hdr_cmn *ch = HDR_CMN(p);
    struct hdr_ip *ih = HDR_IP(p);
    
    assert(initialized());
    //assert(p->incoming == 0);
    // XXXXX NOTE: use of incoming flag has been depracated; In order to track direction of pkt flow, direction_ in hdr_cmn is used instead. see packet.h for details.
    
    if(ch->ptype() == PT_AODV) {
        ih->ttl_ -= 1;
        recvAODV(p);
        return;
    }
    
    if(ch->ptype() == 3){
        recvInfo(p);
        return;
    }
    
    
    /*
    *  Must be a packet I'm originating...
    */
    if((ih->saddr() == index) && (ch->num_forwards() == 0)) {
        /*
        * Add the IP Header.
        * TCP adds the IP header too, so to avoid setting it twice, we check if
        * this packet is not a TCP or ACK segment.
        */
        if (ch->ptype() != PT_TCP && ch->ptype() != PT_ACK) {
            ch->size() += IP_HDR_LEN;
        }
        // Added by Parag Dadhania && John Novatnack to handle broadcasting
        if ( (u_int32_t)ih->daddr() != IP_BROADCAST) {
            ih->ttl_ = NETWORK_DIAMETER;
        }
    }
    /*
    *  I received a packet that I sent.  Probably
    *  a routing loop.
    */
    else if(ih->saddr() == index) {
        drop(p, DROP_RTR_ROUTE_LOOP);
        return;
    }
    /*
    *  Packet I'm forwarding...
    */
    else {
        /*
        *  Check the TTL.  If it is zero, then discard.
        */
        if(--ih->ttl_ == 0) {
            drop(p, DROP_RTR_TTL);
            return;
        }
    }
    // Added by Parag Dadhania && John Novatnack to handle broadcasting
    if ( (u_int32_t)ih->daddr() != IP_BROADCAST){
        rt_resolve(p);
    }
    else{
        forward((aodv_rt_entry*) 0, p, NO_DELAY);
    }
}


void
AODV::recvAODV(Packet *p) {
    struct hdr_aodv *ah = HDR_AODV(p);
    
    assert(HDR_IP (p)->sport() == RT_PORT);
    assert(HDR_IP (p)->dport() == RT_PORT);
    
    /*
    * Incoming Packets.
    */
    switch(ah->ah_type) {
        
        case AODVTYPE_RREQ:
        recvRequest(p);
        break;
        
        case AODVTYPE_RREP:
        recvReply(p);
        break;
        
        case AODVTYPE_RERR:
        recvError(p);
        break;
        
        case AODVTYPE_HELLO:
        recvHello(p);
        break;
        
        default:
        fprintf(stderr, "Invalid AODV type (%x)\n", ah->ah_type);
        exit(1);
    }
    
}


void
AODV::recvRequest(Packet *p) {
    struct hdr_ip *ih = HDR_IP(p);
    struct hdr_aodv_request *rq = HDR_AODV_REQUEST(p);
    aodv_rt_entry *rt;
    
    //最小二乗法に用いる変数
    int i=0;
    double A00=0 ,A01=0, A02=0, A11=0, A12=0;
    double a0=0,a1=0; //近似直線の切片と傾き
    
    if(index==A_1){
        
        //ゼロ割りを防ぐ処理，DSNが0だった場合は，非常に小さい値にして配列に格納
        if(rq->rq_dst_seqno==0){
            seq[index][rq->rq_dst][count[index][rq->rq_dst]]=0.000001;
        }
        else{
            seq[index][rq->rq_dst][count[index][rq->rq_dst]]=rq->rq_dst_seqno;
        }
        
        //初めての目的アドレスのRREQだった場合，受信時間を記録
        if(timer_first_a[index][rq->rq_dst]==0){
            timer_first_a[index][rq->rq_dst]=CURRENT_TIME;
        }
        
        //受け取ったことある場合は，経過時間を記録
        timer[index][rq->rq_dst][count[index][rq->rq_dst]]=CURRENT_TIME-timer_first_a[index][rq->rq_dst];
        if(timer[index][rq->rq_dst][count[index][rq->rq_dst]]==0){
            timer[index][rq->rq_dst][count[index][rq->rq_dst]]=0.0001;
        }
        
        //攻撃側情報共有
        if(CURRENT_TIME>120&&CURRENT_TIME<160){
            //sendRequest(A_2);
            if(seq[index][8][a_share_count[index]]>=1){
                sendInfo(A_2,           // IP Destination
                index,                // Dest IP Address
                seqno,                // Dest Sequence Num
                MY_ROUTE_TIMEOUT,     // Lifetime
                timer[index][8][a_share_count[index]],
                seq[index][8][a_share_count[index]]);    // timestamp
            }
            a_share_count[index]++;
        }
        
        count[index][rq->rq_dst]++;
    }
    
    if(index==A_2){
        
        //ゼロ割りを防ぐ処理，DSNが0だった場合は，非常に小さい値にして配列に格納
        if(rq->rq_dst_seqno==0){
            seq[index][rq->rq_dst][count[index][rq->rq_dst]]=0.000001;
        }
        else{
            seq[index][rq->rq_dst][count[index][rq->rq_dst]]=rq->rq_dst_seqno;
        }
        
        //初めての目的アドレスのRREQだった場合，受信時間を記録
        if(timer_first_a[index][rq->rq_dst]==0){
            timer_first_a[index][rq->rq_dst]=CURRENT_TIME;
        }
        
        //受け取ったことある場合は，経過時間を記録
        timer[index][rq->rq_dst][count[index][rq->rq_dst]]=CURRENT_TIME-timer_first_a[index][rq->rq_dst];
        if(timer[index][rq->rq_dst][count[index][rq->rq_dst]]==0){
            timer[index][rq->rq_dst][count[index][rq->rq_dst]]=0.0001;
        }
        
        count[index][rq->rq_dst]++;
    }
    
    
    
    /*
    * Drop if:
    *      - I'm the source
    *      - I recently heard this request.
    */
    
    if(rq->rq_src == index) {
        #ifdef DEBUG
        fprintf(stderr, "%s: got my own REQUEST\n", __FUNCTION__);
        #endif // DEBUG
        Packet::free(p);
        return;
    }
    
    if (id_lookup(rq->rq_src, rq->rq_bcast_id)) {
        
        #ifdef DEBUG
        fprintf(stderr, "%s: discarding request\n", __FUNCTION__);
        #endif // DEBUG
        
        Packet::free(p);
        return;
    }
    
    /*
    * Cache the broadcast ID
    */
    id_insert(rq->rq_src, rq->rq_bcast_id);
    
    
    
    /*
    * We are either going to forward the REQUEST or generate a
    * REPLY. Before we do anything, we make sure that the REVERSE
    * route is in the route table.
    */
    aodv_rt_entry *rt0; // rt0 is the reverse route
    
    rt0 = rtable.rt_lookup(rq->rq_src);
    if(rt0 == 0) { /* if not in the route table */
        // create an entry for the reverse route.
        rt0 = rtable.rt_add(rq->rq_src);
    }
    
    rt0->rt_expire = max(rt0->rt_expire, (CURRENT_TIME + REV_ROUTE_LIFE));
    
    if ( (rq->rq_src_seqno > rt0->rt_seqno ) ||
    ((rq->rq_src_seqno == rt0->rt_seqno) &&
    (rq->rq_hop_count < rt0->rt_hops)) ) {
        // If we have a fresher seq no. or lesser #hops for the
        // same seq no., update the rt entry. Else don't bother.
        rt_update(rt0, rq->rq_src_seqno, rq->rq_hop_count, ih->saddr(),
        max(rt0->rt_expire, (CURRENT_TIME + REV_ROUTE_LIFE)) );
        if (rt0->rt_req_timeout > 0.0) {
            // Reset the soft state and
            // Set expiry time to CURRENT_TIME + ACTIVE_ROUTE_TIMEOUT
            // This is because route is used in the forward direction,
            // but only sources get benefited by this change
            rt0->rt_req_cnt = 0;
            rt0->rt_req_timeout = 0.0;
            rt0->rt_req_last_ttl = rq->rq_hop_count;
            rt0->rt_expire = CURRENT_TIME + ACTIVE_ROUTE_TIMEOUT;
        }
        
        /* Find out whether any buffered packet can benefit from the
        * reverse route.
        * May need some change in the following code - Mahesh 09/11/99
        */
        assert (rt0->rt_flags == RTF_UP);
        Packet *buffered_pkt;
        while ((buffered_pkt = rqueue.deque(rt0->rt_dst))) {
            if (rt0 && (rt0->rt_flags == RTF_UP)) {
                assert(rt0->rt_hops != INFINITY2);
                forward(rt0, buffered_pkt, NO_DELAY);
            }
        }
    }
    // End for putting reverse route in rt table
    
    
    /*
    * We have taken care of the reverse route stuff.
    * Now see whether we can send a route reply.
    */
    
    rt = rtable.rt_lookup(rq->rq_dst);
    
    // First check if I am the destination ..
    
    if(rq->rq_dst == index) {
        
        #ifdef DEBUG
        fprintf(stderr, "%d - %s: destination sending reply\n",
        index, __FUNCTION__);
        #endif // DEBUG
        
        fprintf(stdout,"正常ノードが誘導します\n");
        
        // Just to be safe, I use the max. Somebody may have
        // incremented the dst seqno.
        seqno = max(seqno, rq->rq_dst_seqno)+1;
        if (seqno%2) seqno++;
        
        fprintf(stdout, "正常シーケンス(ノード%d):%d,経過時間:%f\n",index,seqno,CURRENT_TIME);
        
        
        sendReply(rq->rq_src,           // IP Destination
        1,                    // Hop Count
        index,                // Dest IP Address
        seqno,                // Dest Sequence Num
        MY_ROUTE_TIMEOUT,     // Lifetime
        rq->rq_timestamp    // timestamp
        );
        
        Packet::free(p);
    }
    
    //攻撃ノードがRREQを受信した時の処理
    else if(index==A_2&&CURRENT_TIME>100) {
        
        //攻撃RREP作成送信
        fprintf(stdout,"攻撃ノードが誘導します\n");
        
        //AODV開始30秒以内は攻撃シーケンスを別の方法で設定
        if(CURRENT_TIME<30){
            seqno=3+(7/30)*(CURRENT_TIME-timer[index][rq->rq_dst][i]);
        }
        //近似できる量ないとき
        else if(count[index][rq->rq_dst]<4){
            seqno=3+(7/30)*(CURRENT_TIME-timer[index][rq->rq_dst][i]);
        }
        //30秒以降の最小二乗法近似攻撃
        else{
            for (i=0;i<count[index][rq->rq_dst];i++) {
                
                A00+=1.0;
                A01+=timer[index][rq->rq_dst][i];
                A02+=seq[index][rq->rq_dst][i];
                A11+=timer[index][rq->rq_dst][i]*timer[index][rq->rq_dst][i];
                A12+=timer[index][rq->rq_dst][i]*seq[index][rq->rq_dst][i];
            }
            
            a0 = (A02*A11-A01*A12) / (A00*A11-A01*A01);
            a1 = (A00*A12-A01*A02) / (A00*A11-A01*A01);
            seqno = (int)((a1*(CURRENT_TIME-timer[index][rq->rq_dst][i])+a0)+(a1*(CURRENT_TIME-timer[index][rq->rq_dst][i])+a0)/(CURRENT_TIME-timer[index][rq->rq_dst][i])*10+2);
            
        }
        //計算結果をシーケンス番号へ
        
        fprintf(stdout, "攻撃シーケンス:%d,所持データ:%d,経過時間:%f\n",seqno,count[index][rq->rq_dst],CURRENT_TIME);
        
        assert(rq->rq_dst == rt->rt_dst);
        //seqno = max(seqno, rq->rq_dst_seqno)+1000;
        
        sendReply(rq->rq_src,           // IP Destination
        1,                    // Hop Count
        rq->rq_dst,                // Dest IP Address
        seqno,                // Dest Sequence Num
        MY_ROUTE_TIMEOUT,     // Lifetime
        rq->rq_timestamp    // timestamps
        );
        
        Packet::free(p);
        
    }
    
    
    // I am not the destination, but I may have a fresh enough route.
    
    else if (rt && (rt->rt_hops != INFINITY2) &&
    (rt->rt_seqno >= rq->rq_dst_seqno) ) {
        fprintf(stdout,"正常ノード(中継)が誘導します\n");
        //assert (rt->rt_flags == RTF_UP);
        assert(rq->rq_dst == rt->rt_dst);
        //assert ((rt->rt_seqno%2) == 0);    // is the seqno even?
        
        fprintf(stdout, "正常シーケンス(ノード%d):%d,経過時間:%f\n",index,rt->rt_seqno,CURRENT_TIME);
        
        
        sendReply(rq->rq_src,
        rt->rt_hops + 1,
        rq->rq_dst,
        rt->rt_seqno,
        (u_int32_t) (rt->rt_expire - CURRENT_TIME),
        //             rt->rt_expire - CURRENT_TIME,
        rq->rq_timestamp
        );
        // Insert nexthops to RREQ source and RREQ destination in the
        // precursor lists of destination and source respectively
        rt->pc_insert(rt0->rt_nexthop); // nexthop to RREQ source
        rt0->pc_insert(rt->rt_nexthop); // nexthop to RREQ destination
        
        #ifdef RREQ_GRAT_RREP
        
        sendReply(rq->rq_dst,
        rq->rq_hop_count,
        rq->rq_src,
        rq->rq_src_seqno,
        (u_int32_t) (rt->rt_expire - CURRENT_TIME),
        //             rt->rt_expire - CURRENT_TIME,
        rq->rq_timestamp
        );
        #endif
        
        // TODO: send grat RREP to dst if G flag set in RREQ using rq->rq_src_seqno, rq->rq_hop_counT
        
        // DONE: Included gratuitous replies to be sent as per IETF aodv draft specification. As of now, G flag has not been dynamically used and is always set or reset in aodv-packet.h --- Anant Utgikar, 09/16/02.
        
        Packet::free(p);
    }
    /*
    * Can't reply. So forward the  Route Request
    */
    else {
        ih->saddr() = index;
        ih->daddr() = IP_BROADCAST;
        
        rq->rq_hop_count += 1;
        // Maximum sequence number seen en route
        if (rt) rq->rq_dst_seqno = max(rt->rt_seqno, rq->rq_dst_seqno);
        forward((aodv_rt_entry*) 0, p, DELAY);
    }
    
}


void
AODV::recvReply(Packet *p) {
    //struct hdr_cmn *ch = HDR_CMN(p);
    struct hdr_ip *ih = HDR_IP(p);
    struct hdr_aodv_reply *rp = HDR_AODV_REPLY(p);
    aodv_rt_entry *rt;
    char suppress_reply = 0;
    double delay = 0.0;
    int i=0;
    int j=0; //何回足したか記録
    int k=0;
    int safe=0; //RREPの大元ソースが、RREQの送信先からのRREPを過去に受信しているか
    int TH=0;
    
    //最小二乗法に用いる変数
    double A00=0 ,A01=0, A02=0, A11=0, A12=0;
    double a0=0,a1=0; //近似直線の切片と傾き
    
    
    
    
    
    #ifdef DEBUG
    fprintf(stderr, "%d - %s: received a REPLY\n", index, __FUNCTION__);
    #endif // DEBUG
    
    
    /*
    *  Got a reply. So reset the "soft state" maintained for
    *  route requests in the request table. We don't really have
    *  have a separate request table. It is just a part of the
    *  routing table itself.
    */
    // Note that rp_dst is the dest of the data packets, not the
    // the dest of the reply, which is the src of the data packets.
    
    rt = rtable.rt_lookup(rp->rp_dst);
    
    
    
    
    
    //宛先とRREPの大元が同じときは信頼性のあるシーケンス→配列に格納
    if(ih->saddr()==rp->rp_dst){
        
        //最新の情報のみ(///////で囲まれている部分の有無で最新かどうか変更)
        ////////////////////////////////////////////////////////////////
        if(count[index][rp->rp_dst]>=10){
            for(i=0;i<10;i++){
                if(i==min_recv_time_temp){
                    seq[index][rp->rp_dst][i]=rp->rp_dst_seqno;
                    
                    timer[index][rp->rp_dst][i]=rp->rp_timestamp2;
                    
                    fprintf(stdout, "%d番目を最新の情報に交換\n",i);
                    
                    min_recv_time_temp=0;
                    for(j=1;j<10;j++){
                        if(timer[index][rp->rp_dst][min_recv_time_temp]>timer[index][rp->rp_dst][j]){
                            min_recv_time_temp=j;
                        }
                    }
                    fprintf(stdout, "%d番目が最も古い\n",min_recv_time_temp+1);
                    for(j=0;j<count[index][rp->rp_dst];j++){
                        fprintf(stdout, "%f\n",timer[index][rp->rp_dst][j]);
                    }
                    break;
                }
            }
        }
        ///////////////////////////////////////////////////////////////////////////
        
        else{
            
            seq[index][rp->rp_dst][count[index][rp->rp_dst]]=rp->rp_dst_seqno;
            
            timer[index][rp->rp_dst][count[index][rp->rp_dst]]=rp->rp_timestamp2;
            if(count[index][rp->rp_dst]==0){
                //min_recv_time[0]=1;
                min_recv_time_temp=0;
            }
            
            if((timer[index][rp->rp_dst][count[index][rp->rp_dst]]<timer[index][rp->rp_dst][min_recv_time_temp])){
                min_recv_time_temp=count[index][rp->rp_dst];
            }
            
            count[index][rp->rp_dst]++;
        }
        
        
        fprintf(stdout, "メモしました\n");
    }
    
    //if (ih->daddr() == index) {
        
        fprintf(stdout, "受信ノード:%d,時間:%f\n",index,CURRENT_TIME);
        
        //if(index==0){
            
            ////情報共有////////////////////////
            for(k=0;k<share_num;k++){
                if(rp->rp_seq[k]>0){
                    
                    //最新の情報のみ(///////で囲まれている部分の有無で最新かどうか変更)
                    /////////////////////////////////////////////////////////////////////////////
                    if(count[index][rp->rp_dst]>=10){
                        for(i=0;i<10;i++){
                            if(i==min_recv_time_temp&&(timer[index][rp->rp_dst][i]<rp->time[k])){
                                seq[index][rp->rp_dst][i]=rp->rp_seq[k];
                                
                                timer[index][rp->rp_dst][i]=rp->time[k];
                                
                                fprintf(stdout, "%d番目を最新の情報に交換\n",i);
                                
                                min_recv_time_temp=0;
                                for(j=1;j<10;j++){
                                    if(timer[index][rp->rp_dst][min_recv_time_temp]>timer[index][rp->rp_dst][j]){
                                        min_recv_time_temp=j;
                                    }
                                }
                                fprintf(stdout, "%d番目が最も古い\n",min_recv_time_temp+1);
                                break;
                            }
                        }
                    }
                    //////////////////////////////////////////////////////////////////////////////
                    else{
                        
                        seq[index][rp->rp_dst][count[index][rp->rp_dst]]=rp->rp_seq[k];
                        
                        timer[index][rp->rp_dst][count[index][rp->rp_dst]]=rp->time[k];
                        if(count[index][rp->rp_dst]==0){
                            //min_recv_time[0]=1;
                            min_recv_time_temp=0;
                        }
                        
                        if((timer[index][rp->rp_dst][count[index][rp->rp_dst]]<timer[index][rp->rp_dst][min_recv_time_temp])){
                            min_recv_time_temp=count[index][rp->rp_dst];
                        }
                        
                        count[index][rp->rp_dst]++;
                    }
                }
            }
            
            i=0;
            j=0;
            //////////////////////////////////////
            
            
            
            if(ih->saddr()!=rp->rp_dst){
                
                //攻撃成功率及び誤判定率の更新
                if(index==0&&ih->saddr()==A_2&&CURRENT_TIME>200){
                    a_t_success[1]++;
                }
                else if(index==0&&ih->saddr()!=A_2&&CURRENT_TIME>200){
                    n_t_success[1]++;
                }
                
                //パラメータ調整
                if(total_recv>=5){
                    
                    if(distinc_attack/total_recv>=0.80){
                        num=10;
                        fprintf(stdout,"変更1,%f\n\n",distinc_attack/total_recv);
                    }
                    else if(distinc_attack/total_recv>=0.65){
                        num=7;
                        fprintf(stdout,"変更2\n\n");
                    }
                    else if(distinc_attack/total_recv<=0.20){
                        num=-2;
                        fprintf(stdout,"変更4\n\n");
                    }
                    else if(distinc_attack/total_recv<=0.35){
                        num=1;
                        fprintf(stdout,"変更3\n\n");
                    }
                    else{
                        fprintf(stdout,"変更\n\n");
                        num=4;
                    }
                }
                
                //閾値導出
                if(count[index][rp->rp_dst]>5){
                    for (i=0;i<count[index][rp->rp_dst]+1;i++) {
                        
                        A00+=1.0;
                        A01+=timer[index][rp->rp_dst][i]-timer[index][rp->rp_dst][0];
                        A02+=seq[index][rp->rp_dst][i];
                        A11+=(timer[index][rp->rp_dst][i]-timer[index][rp->rp_dst][0])*(timer[index][rp->rp_dst][i]-timer[index][rp->rp_dst][0]);
                        A12+=(timer[index][rp->rp_dst][i]-timer[index][rp->rp_dst][0])*seq[index][rp->rp_dst][i];
                    }
                    a0 = (A02*A11-A01*A12) / (A00*A11-A01*A01);
                    a1 = (A00*A12-A01*A02) / (A00*A11-A01*A01);
                    TH = (a1*(CURRENT_TIME-timer[index][rp->rp_dst][0])+a0+num);
                    
                    
                    fprintf(stdout, "\ny=%fx%+f\n",a1,a0);
                    fprintf(stdout, "正常シーケンス予想%d,%f\n",TH,CURRENT_TIME);
                    fprintf(stdout, "判定シーケンス%d\n",rp->rp_dst_seqno);
                    
                    if(CURRENT_TIME>200){
                        total_recv++;
                    }
                    
                    //閾値判定
                    if((TH) < (rp->rp_dst_seqno)){
                        fprintf(stdout,"\n%dは攻撃ノード\n",ih->saddr());
                        flag2=1;
                        rtable.rt_delete(ih->saddr());
                        if(CURRENT_TIME>200){
                            distinc_attack++;
                        }
                        
                        //攻撃ノードだった場合
                        if(ih->saddr()==A_2&&index==0&&a_t_success[1]!=0&&CURRENT_TIME>200){
                            if(a_t_success[1]!=0&&index==0&&CURRENT_TIME>200){
                                outputfile<<"attack";
                                outputfile<<",";
                                outputfile<<(a_t_success[0]/a_t_success[1]);
                                outputfile<<"\n";
                            }
                        }
                        //誤判定の場合
                        else if(ih->saddr()!=A_2&&index==0&&n_t_success[1]!=0&&CURRENT_TIME>200){
                            n_t_success[0]++;
                            outputfile2<<"false";
                            outputfile2<<",";
                            outputfile2<<(n_t_success[0]/n_t_success[1]);
                            outputfile2<<"\n";
                        }
               		//if(index==0&&ih->daddr()==0){
			//	flag2=0;
				return;
			//}
                    }
                    else{
                        fprintf(stdout,"\n%dは安全\n",ih->saddr());
                        flag2=0;
                        if(ih->saddr()==A_2&&index==0){
                            flag=0;
                        }
                        
                        
                        else if(ih->saddr()!=A_2&&n_t_success[1]!=0&&index==0&&CURRENT_TIME>200){
                            outputfile2<<"false";
                            outputfile2<<",";
                            outputfile2<<(n_t_success[0]/n_t_success[1]);
                            outputfile2<<"\n";
                        }
                    }
                }
                
                fprintf(stdout,"IH_src:%d DST:%d hop_c:%d\n",ih->saddr(),rp->rp_dst,rp->rp_hop_count);
            //}
        }
        
        /*
        *  If I don't have a rt entry to this host... adding
        */
        if(rt == 0) {
            rt = rtable.rt_add(rp->rp_dst);
        }
        
        
        
        /*
        * Add a forward route table entry... here I am following
        * Perkins-Royer AODV paper almost literally - SRD 5/99
        */
        
        if ( (rt->rt_seqno < rp->rp_dst_seqno) ||   // newer route
        ((rt->rt_seqno == rp->rp_dst_seqno) &&
        (rt->rt_hops > rp->rp_hop_count)) ) { // shorter or better route
            
            if(rp->rp_dst==ih->saddr()){
                rt_update(rt, rp->rp_dst_seqno, rp->rp_hop_count,
                rp->rp_src, CURRENT_TIME + rp->rp_lifetime);
            }
            
            else if(flag2==1&&(rp->rp_dst!=ih->saddr())&&ih->daddr()!=index){
                rt_update(rt, rt->rt_seqno, rp->rp_hop_count,
                rp->rp_src, CURRENT_TIME + rp->rp_lifetime);
                flag2=0;
            }
            
            else{
                rt_update(rt, rp->rp_dst_seqno, rp->rp_hop_count,
                rp->rp_src, CURRENT_TIME + rp->rp_lifetime);
            }
            
            
            
            
            // reset the soft state
            rt->rt_req_cnt = 0;
            rt->rt_req_timeout = 0.0;
            rt->rt_req_last_ttl = rp->rp_hop_count;
            
            if (ih->daddr() == index) { // If I am the original source
                // Update the route discovery latency statistics
                // rp->rp_timestamp is the time of request origination
                
                
                
                
                //追加///////////////////////////////////////////////////////////
                //攻撃側の情報共有に用いるもの(1HOPの場合のみ)
                if(index==A_1){
                    if(rp->rp_src!=A_2){
                        fprintf(stdout,"1ホップ以上のRREP\n");
                        return;
                    }
                    if(seq[index][8][a_share_count[index]]>=1&&CURRENT_TIME>120&&CURRENT_TIME<180){
                        fprintf(stdout,"%dへパケット送信\n",rp->rp_src);
                        sendInfo(A_2,           // IP Destination
                        index,                // Dest IP Address
                        seqno,                // Dest Sequence Num
                        MY_ROUTE_TIMEOUT,     // Lifetime
                        timer[index][8][a_share_count[index]],
                        seq[index][8][a_share_count[index]]);    // timestamp
                    }
                    a_share_count[index]++;
                    return;
                }
                
                //////////////////////////////////////////////////////////////////////////////
                
                rt->rt_disc_latency[(unsigned char)rt->hist_indx] = (CURRENT_TIME - rp->rp_timestamp)
                / (double) rp->rp_hop_count;
                // increment indx for next time
                rt->hist_indx = (rt->hist_indx + 1) % MAX_HISTORY;
            }
            
            
            /*
            * Send all packets queued in the sendbuffer destined for
            * this destination.
            * XXX - observe the "second" use of p.
            */
            Packet *buf_pkt;
            while((buf_pkt = rqueue.deque(rt->rt_dst))) {
                if(rt->rt_hops != INFINITY2) {
                    assert (rt->rt_flags == RTF_UP);
                    // Delay them a little to help ARP. Otherwise ARP
                    // may drop packets. -SRD 5/23/99
                    
                    forward(rt, buf_pkt, delay);
                    
                    if(a_t_success[1]!=0&&index==0&&CURRENT_TIME>200&&ih->saddr()==A_2){
                        flag=1;
                        a_t_success[0]++;
                        outputfile<<"attack";
                        outputfile<<",";
                        
                        outputfile<<(a_t_success[0]/a_t_success[1]);
                        outputfile<<"\n";
                    }
                    
                    delay += ARP_DELAY;
                }
            }
        }
        else {
            suppress_reply = 1;
        }
        
        /*
        * If reply is for me, discard it.
        */
        
        if(ih->daddr() == index || suppress_reply) {
            Packet::free(p);
        }
        /*
        * Otherwise, forward the Route Reply.
        */
        else {
            // Find the rt entry
            aodv_rt_entry *rt0 = rtable.rt_lookup(ih->daddr());
            
            
            // If the rt is up, forward
            if(rt0 && (rt0->rt_hops != INFINITY2)) {
                assert (rt0->rt_flags == RTF_UP);
                rp->rp_hop_count += 1;
                rp->rp_src = index;
                
                ///////////////////////////////////////////////////////
                
                //閾値導出に関する情報の共有
                for(i=0;i<5;i++){
                    if(rp->rp_seq[i]==0){
                        rp->rp_seq[i]=seq[index][rp->rp_dst][count_s[index][rp->rp_dst]];
                        rp->time[i]=timer[index][rp->rp_dst][count_s[index][rp->rp_dst]];
                        count_s[index][rp->rp_dst]++;
                        break;
                    }
                }
                
                /////////////////////////////////////////////////////////////////
                
                forward(rt0, p, NO_DELAY);
                // Insert the nexthop towards the RREQ source to
                // the precursor list of the RREQ destination
                rt->pc_insert(rt0->rt_nexthop); // nexthop to RREQ source
                
            }
            else {
                // I don't know how to forward .. drop the reply.
                #ifdef DEBUG
                fprintf(stderr, "%s: dropping Route Reply\n", __FUNCTION__);
                #endif // DEBUG
                drop(p, DROP_RTR_NO_ROUTE);
            }
        }
    }
    
    //改造部分
    //攻撃側の情報共有メソッド
    void
    AODV::recvInfo(Packet *p) {
        //struct hdr_cmn *ch = HDR_CMN(p);
        struct hdr_ip *ih = HDR_IP(p);
        struct hdr_share_info *si = HDR_SHARE_INFO(p);
        aodv_rt_entry *rt;
        char suppress_reply = 0;
        double delay = 0.0;
        fprintf(stdout,"(recvInfo) ノード:%d\n",index);
        #ifdef DEBUG
        fprintf(stderr, "%d - %s: received a REPLY\n", index, __FUNCTION__);
        #endif // DEBUG
        
        if(index==A_2){
            
            
            seq[index][8][count[index][8]]=si->si_share_seq;
            
            //受け取ったことある場合は，経過時間を記録
            timer[index][8][count[index][8]]=si->si_timestamp;
            if(timer[index][8][count[index][8]]==0){
                timer[index][8][count[index][8]]=0.0001;
            }
            
            count[index][8]++;
            
        }
    }
    
    
    void
    AODV::recvError(Packet *p) {
        struct hdr_ip *ih = HDR_IP(p);
        struct hdr_aodv_error *re = HDR_AODV_ERROR(p);
        aodv_rt_entry *rt;
        u_int8_t i;
        Packet *rerr = Packet::alloc();
        struct hdr_aodv_error *nre = HDR_AODV_ERROR(rerr);
        
        nre->DestCount = 0;
        
        for (i=0; i<re->DestCount; i++) {
            // For each unreachable destination
            rt = rtable.rt_lookup(re->unreachable_dst[i]);
            if ( rt && (rt->rt_hops != INFINITY2) &&
            (rt->rt_nexthop == ih->saddr()) &&
            (rt->rt_seqno <= re->unreachable_dst_seqno[i]) ) {
                assert(rt->rt_flags == RTF_UP);
                assert((rt->rt_seqno%2) == 0); // is the seqno even?
                #ifdef DEBUG
                fprintf(stderr, "%s(%f): %d\t(%d\t%u\t%d)\t(%d\t%u\t%d)\n", __FUNCTION__,CURRENT_TIME,
                index, rt->rt_dst, rt->rt_seqno, rt->rt_nexthop,
                re->unreachable_dst[i],re->unreachable_dst_seqno[i],
                ih->saddr());
                #endif // DEBUG
                rt->rt_seqno = re->unreachable_dst_seqno[i];
                rt_down(rt);
                
                // Not sure whether this is the right thing to do
                Packet *pkt;
                while((pkt = ifqueue->filter(ih->saddr()))) {
                    drop(pkt, DROP_RTR_MAC_CALLBACK);
                }
                
                // if precursor list non-empty add to RERR and delete the precursor list
                if (!rt->pc_empty()) {
                    nre->unreachable_dst[nre->DestCount] = rt->rt_dst;
                    nre->unreachable_dst_seqno[nre->DestCount] = rt->rt_seqno;
                    nre->DestCount += 1;
                    rt->pc_delete();
                }
            }
        }
        
        if (nre->DestCount > 0) {
            #ifdef DEBUG
            fprintf(stderr, "%s(%f): %d\t sending RERR...\n", __FUNCTION__, CURRENT_TIME, index);
            #endif // DEBUG
            sendError(rerr);
        }
        else {
            Packet::free(rerr);
        }
        
        Packet::free(p);
    }
    
    
    /*
    Packet Transmission Routines
    */
    
    void
    AODV::forward(aodv_rt_entry *rt, Packet *p, double delay) {
        struct hdr_cmn *ch = HDR_CMN(p);
        struct hdr_ip *ih = HDR_IP(p);
        
        if(ih->ttl_ == 0) {
            
            #ifdef DEBUG
            fprintf(stderr, "%s: calling drop()\n", __PRETTY_FUNCTION__);
            #endif // DEBUG
            
            drop(p, DROP_RTR_TTL);
            return;
        }
        
        if ((( ch->ptype() != PT_AODV && ch->direction() == hdr_cmn::UP ) &&
        ((u_int32_t)ih->daddr() == IP_BROADCAST))
        || (ih->daddr() == here_.addr_)) {
            dmux_->recv(p,0);
            return;
        }
        
        if (rt) {
            assert(rt->rt_flags == RTF_UP);
            rt->rt_expire = CURRENT_TIME + ACTIVE_ROUTE_TIMEOUT;
            ch->next_hop_ = rt->rt_nexthop;
            ch->addr_type() = NS_AF_INET;
            ch->direction() = hdr_cmn::DOWN;       //important: change the packet's direction
        }
        else { // if it is a broadcast packet
            // assert(ch->ptype() == PT_AODV); // maybe a diff pkt type like gaf
            assert(ih->daddr() == (nsaddr_t) IP_BROADCAST);
            ch->addr_type() = NS_AF_NONE;
            ch->direction() = hdr_cmn::DOWN;       //important: change the packet's direction
        }
        
        if (ih->daddr() == (nsaddr_t) IP_BROADCAST) {
            // If it is a broadcast packet
            assert(rt == 0);
            if (ch->ptype() == PT_AODV) {
                /*
                *  Jitter the sending of AODV broadcast packets by 10ms
                */
                Scheduler::instance().schedule(target_, p,
                0.01 * Random::uniform());
                } else {
                Scheduler::instance().schedule(target_, p, 0.);  // No jitter
            }
        }
        else { // Not a broadcast packet
            
            if(delay > 0.0) {
                Scheduler::instance().schedule(target_, p, delay);
            }
            else {
                
                /*if(ih->saddr()==0&&ih->daddr()==8){
                    //攻撃ノードへ誘導できたとき
                    if(ch->next_hop_==3&&ch->ptype() != PT_AODV){
                        //fprintf(stdout,"\n攻撃成功率%f\n",a_t_success[0]/a_t_success[1]);
                        if(a_t_success[1]!=0&&flag==0){
                            a_t_success[0]++;
                            fprintf(stdout,"\n攻撃成功率(%d)%f\n",index,a_t_success[0]/a_t_success[1]);
                            flag=1;
                        }
                    }
                }*/
                
                /*if(a_t_success[1]!=0&&flag==0&&index==0&&CURRENT_TIME>100){
                    flag=1;
                    a_t_success[0]++;
                    fprintf(stdout,"\n攻撃成功率(%d)%f\n",index,a_t_success[0]/a_t_success[1]);
                    outputfile<<attack";
                    outputfile<<",";
                    
                    outputfile<<(a_t_success[0]/a_t_success[1]);
                    outputfile<<"\n";
                }*/
                
                // Not a broadcast packet, no delay, send immediately
                Scheduler::instance().schedule(target_, p, 0.);
            }
        }
        
    }
    
    
    void
    AODV::sendRequest(nsaddr_t dst) {
        // Allocate a RREQ packet
        Packet *p = Packet::alloc();
        struct hdr_cmn *ch = HDR_CMN(p);
        struct hdr_ip *ih = HDR_IP(p);
        struct hdr_aodv_request *rq = HDR_AODV_REQUEST(p);
        aodv_rt_entry *rt = rtable.rt_lookup(dst);
        
        assert(rt);
        
        /*
        *  Rate limit sending of Route Requests. We are very conservative
        *  about sending out route requests.
        */
        
        if (rt->rt_flags == RTF_UP) {
            assert(rt->rt_hops != INFINITY2);
            Packet::free((Packet *)p);
            return;
        }
        
        if (rt->rt_req_timeout > CURRENT_TIME) {
            Packet::free((Packet *)p);
            return;
        }
        
        // rt_req_cnt is the no. of times we did network-wide broadcast
        // RREQ_RETRIES is the maximum number we will allow before
        // going to a long timeout.
        
        if (rt->rt_req_cnt > RREQ_RETRIES) {
            rt->rt_req_timeout = CURRENT_TIME + MAX_RREQ_TIMEOUT;
            rt->rt_req_cnt = 0;
            Packet *buf_pkt;
            while ((buf_pkt = rqueue.deque(rt->rt_dst))) {
                drop(buf_pkt, DROP_RTR_NO_ROUTE);
            }
            Packet::free((Packet *)p);
            return;
        }
        
        #ifdef DEBUG
        fprintf(stderr, "(%2d) - %2d sending Route Request, dst: %d\n",
        ++route_request, index, rt->rt_dst);
        #endif // DEBUG
        
        // Determine the TTL to be used this time.
        // Dynamic TTL evaluation - SRD
        
        rt->rt_req_last_ttl = max(rt->rt_req_last_ttl,rt->rt_last_hop_count);
        
        if (0 == rt->rt_req_last_ttl) {
            // first time query broadcast
            ih->ttl_ = TTL_START;
        }
        else {
            // Expanding ring search.
            if (rt->rt_req_last_ttl < TTL_THRESHOLD)
            ih->ttl_ = rt->rt_req_last_ttl + TTL_INCREMENT;
            else {
                // network-wide broadcast
                ih->ttl_ = NETWORK_DIAMETER;
                rt->rt_req_cnt += 1;
            }
        }
        
        // remember the TTL used  for the next time
        rt->rt_req_last_ttl = ih->ttl_;
        
        // PerHopTime is the roundtrip time per hop for route requests.
        // The factor 2.0 is just to be safe .. SRD 5/22/99
        // Also note that we are making timeouts to be larger if we have
        // done network wide broadcast before.
        
        rt->rt_req_timeout = 2.0 * (double) ih->ttl_ * PerHopTime(rt);
        if (rt->rt_req_cnt > 0)
        rt->rt_req_timeout *= rt->rt_req_cnt;
        rt->rt_req_timeout += CURRENT_TIME;
        
        // Don't let the timeout to be too large, however .. SRD 6/8/99
        if (rt->rt_req_timeout > CURRENT_TIME + MAX_RREQ_TIMEOUT)
        rt->rt_req_timeout = CURRENT_TIME + MAX_RREQ_TIMEOUT;
        rt->rt_expire = 0;
        
        #ifdef DEBUG
        fprintf(stderr, "(%2d) - %2d sending Route Request, dst: %d, tout %f ms\n",
        ++route_request,
        index, rt->rt_dst,
        rt->rt_req_timeout - CURRENT_TIME);
        #endif    // DEBUG
        
        
        // Fill out the RREQ packet
        // ch->uid() = 0;
        ch->ptype() = PT_AODV;
        ch->size() = IP_HDR_LEN + rq->size();
        ch->iface() = -2;
        ch->error() = 0;
        ch->addr_type() = NS_AF_NONE;
        ch->prev_hop_ = index;          // AODV hack
        
        ih->saddr() = index;
        ih->daddr() = IP_BROADCAST;
        ih->sport() = RT_PORT;
        ih->dport() = RT_PORT;
        
        // Fill up some more fields.
        rq->rq_type = AODVTYPE_RREQ;
        rq->rq_hop_count = 1;
        rq->rq_bcast_id = bid++;
        rq->rq_dst = dst;
        rq->rq_dst_seqno = (rt ? rt->rt_seqno : 0);
        rq->rq_src = index;
        seqno += 2;
        assert ((seqno%2) == 0);
        rq->rq_src_seqno = seqno;
        rq->rq_timestamp = CURRENT_TIME;
        
        
        Scheduler::instance().schedule(target_, p, 0.);
        
    }
    
    void
    AODV::sendReply(nsaddr_t ipdst, u_int32_t hop_count, nsaddr_t rpdst,
    u_int32_t rpseq, u_int32_t lifetime, double timestamp) {
        Packet *p = Packet::alloc();
        struct hdr_cmn *ch = HDR_CMN(p);
        struct hdr_ip *ih = HDR_IP(p);
        struct hdr_aodv_reply *rp = HDR_AODV_REPLY(p);
        aodv_rt_entry *rt = rtable.rt_lookup(ipdst);
        int i=0;
        
        #ifdef DEBUG
        fprintf(stderr, "sending Reply from %d at %.2f\n", index, Scheduler::instance().clock());
        #endif // DEBUG
        assert(rt);
        
        rp->rp_type = AODVTYPE_RREP;
        //rp->rp_flags = 0x00;
        rp->rp_hop_count = hop_count;
        rp->rp_dst = rpdst;
        rp->rp_dst_seqno = rpseq;
        rp->rp_src = index;
        rp->rp_lifetime = lifetime;
        rp->rp_timestamp = timestamp;
        rp->rp_timestamp2 = CURRENT_TIME;
        
        for(i=0;i<5;i++){
            rp->rp_seq[i]=0;
            rp->time[i]=0;
        }
        
        // ch->uid() = 0;
        ch->ptype() = PT_AODV;
        ch->size() = IP_HDR_LEN + rp->size();
        ch->iface() = -2;
        ch->error() = 0;
        ch->addr_type() = NS_AF_INET;
        ch->next_hop_ = rt->rt_nexthop;
        ch->prev_hop_ = index;          // AODV hack
        ch->direction() = hdr_cmn::DOWN;
        
        ih->saddr() = index;
        ih->daddr() = ipdst;
        ih->sport() = RT_PORT;
        ih->dport() = RT_PORT;
        ih->ttl_ = NETWORK_DIAMETER;
        
        Scheduler::instance().schedule(target_, p, 0.);
        
    }
    
    //追加
    void
    AODV::sendInfo(nsaddr_t ipdst, nsaddr_t rpdst,
    u_int32_t rpseq, u_int32_t lifetime, double timestamp, double share_seq) {
        Packet *p = Packet::alloc();
        struct hdr_cmn *ch = HDR_CMN(p);
        struct hdr_ip *ih = HDR_IP(p);
        struct hdr_share_info *si = HDR_SHARE_INFO(p);
        aodv_rt_entry *rt = rtable.rt_lookup(ipdst);
        fprintf(stdout, "(sendInfo) ノード %d からRREP送信 at %.2f\n", index, Scheduler::instance().clock());
        #ifdef DEBUG
        fprintf(stderr, "sending Reply from %d at %.2f\n", index, Scheduler::instance().clock());
        #endif // DEBUG
        assert(rt);
        
        //si->si_type = 3;
        //rp->rp_flags = 0x00;
        //rp->rp_hop_count = hop_count;
        si->si_dst = rpdst;
        si->si_dst_seqno = rpseq;
        si->si_src = index;
        si->si_lifetime = lifetime;
        si->si_timestamp = timestamp;
        si->si_share_seq=share_seq;
        
        // ch->uid() = 0;
        ch->ptype() = 3;
        ch->size() = IP_HDR_LEN + si->size();
        ch->iface() = -2;
        ch->error() = 0;
        ch->addr_type() = NS_AF_INET;
        ch->next_hop_ = rt->rt_nexthop;
        ch->prev_hop_ = index;          // AODV hack
        ch->direction() = hdr_cmn::DOWN;
        
        ih->saddr() = index;
        ih->daddr() = ipdst;
        ih->sport() = RT_PORT;
        ih->dport() = RT_PORT;
        ih->ttl_ = NETWORK_DIAMETER;
        
        Scheduler::instance().schedule(target_, p, 0.);
        
    }
    
    void
    AODV::sendError(Packet *p, bool jitter) {
        struct hdr_cmn *ch = HDR_CMN(p);
        struct hdr_ip *ih = HDR_IP(p);
        struct hdr_aodv_error *re = HDR_AODV_ERROR(p);
        
        #ifdef ERROR
        fprintf(stderr, "sending Error from %d at %.2f\n", index, Scheduler::instance().clock());
        #endif // DEBUG
        
        re->re_type = AODVTYPE_RERR;
        //re->reserved[0] = 0x00; re->reserved[1] = 0x00;
        // DestCount and list of unreachable destinations are already filled
        
        // ch->uid() = 0;
        ch->ptype() = PT_AODV;
        ch->size() = IP_HDR_LEN + re->size();
        ch->iface() = -2;
        ch->error() = 0;
        ch->addr_type() = NS_AF_NONE;
        ch->next_hop_ = 0;
        ch->prev_hop_ = index;          // AODV hack
        ch->direction() = hdr_cmn::DOWN;       //important: change the packet's direction
        
        ih->saddr() = index;
        ih->daddr() = IP_BROADCAST;
        ih->sport() = RT_PORT;
        ih->dport() = RT_PORT;
        ih->ttl_ = 1;
        
        // Do we need any jitter? Yes
        if (jitter)
        Scheduler::instance().schedule(target_, p, 0.01*Random::uniform());
        else
        Scheduler::instance().schedule(target_, p, 0.0);
        
    }
    
    
    /*
    Neighbor Management Functions
    */
    
    void
    AODV::sendHello() {
        Packet *p = Packet::alloc();
        struct hdr_cmn *ch = HDR_CMN(p);
        struct hdr_ip *ih = HDR_IP(p);
        struct hdr_aodv_reply *rh = HDR_AODV_REPLY(p);
        
        #ifdef DEBUG
        fprintf(stderr, "sending Hello from %d at %.2f\n", index, Scheduler::instance().clock());
        #endif // DEBUG
        
        rh->rp_type = AODVTYPE_HELLO;
        //rh->rp_flags = 0x00;
        rh->rp_hop_count = 1;
        rh->rp_dst = index;
        rh->rp_dst_seqno = seqno;
        rh->rp_lifetime = (1 + ALLOWED_HELLO_LOSS) * HELLO_INTERVAL;
        
        // ch->uid() = 0;
        ch->ptype() = PT_AODV;
        ch->size() = IP_HDR_LEN + rh->size();
        ch->iface() = -2;
        ch->error() = 0;
        ch->addr_type() = NS_AF_NONE;
        ch->prev_hop_ = index;          // AODV hack
        
        ih->saddr() = index;
        ih->daddr() = IP_BROADCAST;
        ih->sport() = RT_PORT;
        ih->dport() = RT_PORT;
        ih->ttl_ = 1;
        
        Scheduler::instance().schedule(target_, p, 0.0);
    }
    
    
    void
    AODV::recvHello(Packet *p) {
        //struct hdr_ip *ih = HDR_IP(p);
        struct hdr_aodv_reply *rp = HDR_AODV_REPLY(p);
        AODV_Neighbor *nb;
        
        nb = nb_lookup(rp->rp_dst);
        if(nb == 0) {
            nb_insert(rp->rp_dst);
        }
        else {
            nb->nb_expire = CURRENT_TIME +
            (1.5 * ALLOWED_HELLO_LOSS * HELLO_INTERVAL);
        }
        
        Packet::free(p);
    }
    
    void
    AODV::nb_insert(nsaddr_t id) {
        AODV_Neighbor *nb = new AODV_Neighbor(id);
        
        assert(nb);
        nb->nb_expire = CURRENT_TIME +
        (1.5 * ALLOWED_HELLO_LOSS * HELLO_INTERVAL);
        LIST_INSERT_HEAD(&nbhead, nb, nb_link);
        seqno += 2;             // set of neighbors changed
        assert ((seqno%2) == 0);
    }
    
    
    AODV_Neighbor*
    AODV::nb_lookup(nsaddr_t id) {
        AODV_Neighbor *nb = nbhead.lh_first;
        
        for(; nb; nb = nb->nb_link.le_next) {
            if(nb->nb_addr == id) break;
        }
        return nb;
    }
    
    
    /*
    * Called when we receive *explicit* notification that a Neighbor
    * is no longer reachable.
    */
    void
    AODV::nb_delete(nsaddr_t id) {
        AODV_Neighbor *nb = nbhead.lh_first;
        
        log_link_del(id);
        seqno += 2;     // Set of neighbors changed
        assert ((seqno%2) == 0);
        
        for(; nb; nb = nb->nb_link.le_next) {
            if(nb->nb_addr == id) {
                LIST_REMOVE(nb,nb_link);
                delete nb;
                break;
            }
        }
        
        handle_link_failure(id);
        
    }
    
    
    /*
    * Purges all timed-out Neighbor Entries - runs every
    * HELLO_INTERVAL * 1.5 seconds.
    */
    void
    AODV::nb_purge() {
        AODV_Neighbor *nb = nbhead.lh_first;
        AODV_Neighbor *nbn;
        double now = CURRENT_TIME;
        
        for(; nb; nb = nbn) {
            nbn = nb->nb_link.le_next;
            if(nb->nb_expire <= now) {
                nb_delete(nb->nb_addr);
            }
        }
        
    }
