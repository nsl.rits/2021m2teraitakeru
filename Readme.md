【ソフト名】ノード協調によるブラックホール防御プログラム
【作成者】寺井健
【制作日】2021/2/11
【連絡先】tera509bump509@gmail.com
【動作環境】Ubuntu 16.04 LTS
【開発環境】ns-2 ver2.35  サイト"https://www.isi.edu/nsnam/ns/"

・ファイル構成
1.AODVの挙動に関連するファイル(c++ソースファイルおよびそのヘッダファイルなど)
引継ぎ資料内の"siBH"フォルダ(単一BH攻撃に関するもの)や"coBH"フォルダ(協調BH攻撃に関するもの)内にまとめてあります．
これらのファイルは，"~/ns-allinone-2.35/ns-2.35/aodv" 下に置いてください

2.シナリオファイル(TCLファイルなど)
"scenario"フォルダ内の"siBHTCL"や"coBHtcl"内にあります．ファイル名の一部となっている数字はノード数です．
ノード数に応じて必要なシナリオを用いてください．
TCLファイルは，"~/ns-allinone-2.35/ns-2.35/ex/tcl" 下に置いてください.

TCLファイル内にノードの移動やトラフィックを書き込むことも可能ですが，私のTCLファイルにおいては，
別ファイルに記載されているものを読み込んでいます．
ノードの移動は"nodemovement"というファイル名で，トラフィックに関する記述は"traffic"というファイル名にして，
TCLファイルと同一の場所に置いてください(TCLファイル内で読み込むファイルの名前を変更すれば，それ以外の名前でも可能です)．
これらのサンプルも引き継ぎ資料内にあります．

・使い方
1.tclファイル
tclファイルは，ノードの設定(ノード数や移動)や，通信の開始,終了時間といった，シナリオを記述するファイルです．
ノードの移動やリンク設定などは別のファイル(nodemovement, traffic)で設定していますが，tclファイル内でも記述可能です．
詳しい説明はソースコード内に記述しています．
~/ns-allinone-2.34/ns-2.35/ex/tcl で端末を起動し，"ns 'TCLファイル名'"と入力し実行することで，tclファイルが動作します．

2.c++ファイル及びそのヘッダファイル
c++ファイル及びそのヘッダファイルは，ノードの内部的な処理(例えば，RREQパケットを受信したとき，どのような処理を行うかなど)を記述します．
私の研究では，AODVプロトコルの挙動に関するc++ファイルを用いました．
初回は ~/ns-allinone-2.34/ns-2.35　下で ./configureを実行し，コンパイルに必要なファイルを作成し，その後，makeコマンドでコンパイルします．
動作の詳しい説明はソースコード内に記述しています．
私の研究では主に，RREQの送受信，RREPの送受信の部分がメインになります．


ns2の導入については，
水野研 H.K.:
"VMwarePlayer 上の Ubuntu で NS-2 を動作させる手法"
URL"http://www.ey.u-tokai.ac.jp/mizuno/Reference/2010/ubuntu_ns-2_Rev3.pdf"

が参考になります．

ns2の基本的な勉強については，
水野~秀樹:
 ”NS2によるネットワークシミュレーション入門 有線からワイヤレスアドホックネットワークまで”，
森北出版株式会社．

がおすすめです．